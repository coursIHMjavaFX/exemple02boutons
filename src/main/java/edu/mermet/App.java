package edu.mermet;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import java.time.LocalTime;

/**
 * JavaFX App
 */
public class App extends Application {
	@Override
	public void start(Stage stage) {
		var boite = new HBox();
		var bHeure = new Button("Heure");
		var bQuitter = new Button("Quitter");
		var affichage = new Label("                ");
		bQuitter.setOnAction(ev -> System.exit(0));
		bHeure.setOnAction(new AfficherHeure(affichage));
		bHeure.setDefaultButton(true);
		boite.getChildren().addAll(bQuitter, bHeure, affichage);
		var scene = new Scene(boite);
		affichage.requestFocus();
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch();
	}

	class AfficherHeure implements EventHandler<ActionEvent> {
		private Label zoneAffichage;

		public AfficherHeure(Label affichage) {
			zoneAffichage = affichage;
		}

		@Override
		public void handle(ActionEvent ae) {
			var heure = LocalTime.now();
			String texte = heure.getHour() + ":" + heure.getMinute() + ":" + heure.getSecond();
			zoneAffichage.setText(texte);
			System.out.println(texte);
		}
	}

}
